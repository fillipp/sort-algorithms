public class BubbleSorter implements Sorter {
    @Override
    public void sort(int[] toSort) {

        for (int i = 0; i < toSort.length; i++) {
            for (int j = 0; j < toSort.length -1 ; j++) {
                if (toSort[j] > toSort[j +1]){
                    ArrayUtils.swapElements(toSort, j, j+1);
                }

            }

        }

    }

    @Override
    public String getAlgorithmName() {
        return "sortowanie bąbelkowe";
    }
}
