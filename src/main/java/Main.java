import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] ints = ArrayUtils.generateIntegerRandomArray(10);

        System.out.println(Arrays.toString(ints));

        Sorter sorter = new BubbleSorter();
        sorter.sort(ints);

        System.out.println(Arrays.toString(ints));
    }
}
